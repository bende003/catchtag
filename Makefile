#-Usage:	make [target]; available targets:
Usage usage:
	@test -s "$${VIRTUAL_ENV}"	|| echo "Warning: VIRTUAL_ENV not set" >&2
	@sed -n 's/^#-//p' [Mm]akefile

#-all:	make  Qt run
all:	Qt run

#-new:	make  Qt 
new:	Qt 

#-Qt:		make -C Qt_designs/
Qt:
	make -C Qt_designs/

#-run:		run CatchTag in right environment
run: ctags
	PYTHONPATH=src/ python src/catchtag.py

ctags: 
	ctags -R src/

#-pdb:	debug billie with pdb
pdb:
	PYTHONPATH=src/ python -m pdb src/catchtag.py	

#-pudb:	debug billie with pudb
pudb:
	PYTHONPATH=src/ python -m pudb src/catchtag.py	
#-checkin ci:	hg commit and push
checkin ci:
	hg commit
	hg push

#-checkout co:	hg pull
checkout co:
	hg pull
