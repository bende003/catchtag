"""
    This file is part of CatchTag.

    CatchTag is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CatchTag is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import csv
import enum
from PyQt5.QtWidgets import QApplication, QWidget, QGraphicsScene, QGraphicsItem, QFileDialog, QGraphicsView, QGraphicsRectItem, QTableWidgetItem, QHeaderView
from PyQt5.QtCore import Qt, QRectF, QRect, QPointF
from PyQt5.QtGui import QBrush, QPainterPath, QColor, QRadialGradient, QPixmap, QImage, QPainter
from PyQt5.QtChart import QChartView, QChart, QPieSeries
from qt5.QCatchTag import Ui_CatchTag

from models import  FileFault, SpeciesTagsList, Point

class QMainWidget(QWidget):
    def __init__(self, ui_cls, model, parent=None):
        super().__init__(parent)
        self.ui = ui_cls()
        self.ui.setupUi(self)
        self.__model = model
        self.__species_list = model.species_list
        self.__species_icons = {}

        self.scene = CatchTagScene(model)
        self.ui.catchcamView.setScene(self.scene)
        self.ui.species.addItems([""] + model.species_list)
        self.ui.species.currentTextChanged.connect(self.select_species)
        self.ui.catchcamView.setDragMode(QGraphicsView.NoDrag)
        self.chart_view = QChartView()
        self.chart_view.setBackgroundBrush(QBrush(QColor(79,79,79)))
        self.ui.chart_layout.addWidget(self.chart_view)
        self.directory = None
        self.__current_file = None
        self.file_fault = None
        self.__has_image = False
        self.img_rect = None
        self.roi = None
        self.roi_item = None
        self.view_rect = None
        self.zoom_mode = False
        self.report_filename = None 
        self.pos_filename = None
        self.__connect()
        self.__enable_toolbox(False)

    
    def __connect(self):
        self.ui.open.clicked.connect(self.browse)
        self.ui.next_img.clicked.connect(self.next)
        self.ui.prev_img.clicked.connect(self.prev)
        self.ui.zoomin.clicked.connect(self.zoomin)
        self.ui.zoomout.clicked.connect(self.zoomout)
        self.ui.fit.clicked.connect(self.zoomfit)
        self.ui.select.toggled.connect(self.toggle_select)
        self.ui.deletepoints.toggled.connect(self.toggle_delete)
        self.ui.catchcamView.rubberBandChanged.connect(self.select_area)
        self.scene.changed.connect(self.populate_table)
        self.scene.changed.connect(self.create_chart)
        self.scene.changed.connect(self.save)
    
    def __enable_toolbox(self, enabled):
        self.ui.next_img.setEnabled(enabled)
        self.ui.prev_img.setEnabled(enabled)
        self.ui.zoomin.setEnabled(enabled)
        self.ui.zoomout.setEnabled(enabled)
        self.ui.select.setEnabled(enabled)
        self.ui.deletepoints.setEnabled(enabled)


    def populate_table(self, region=None):
        self.ui.tags.clear()
        self.ui.tags.setColumnCount(3)
        count = list(self.__model.count_current())
        self.ui.tags.setRowCount(len(count))
        self.ui.tags.setHorizontalHeaderLabels(["Legend", "Species", "Count"])
        vheader = QHeaderView(Qt.Vertical)
        vheader.hide()
        self.ui.tags.setVerticalHeader(vheader)
        species_list = self.__model.species_list

        for row, c in enumerate(count):
            species_item = QTableWidgetItem(c[0])
            count_item = QTableWidgetItem(str(c[1]))
            color = species_list.get_color(str(c[0]))
            c = QColor(color[0], color[1], color[2])
            legenditem = QTableWidgetItem()
            legenditem.setData(Qt.BackgroundRole, QBrush(c))
            self.ui.tags.setItem(row,0, legenditem)
            self.ui.tags.setItem(row,1, species_item)
            self.ui.tags.setItem(row,2, count_item)
        self.ui.tags.resizeColumnsToContents()

    def create_chart(self, region=None):
        data = self.__model.count_all()
        series = QPieSeries()
        for c in data:
            series.append(str(c[0]), int(c[1]))

        for pslice in series.slices():
            r,g,b  = self.__species_list.get_color(pslice.label())
            pslice.setBrush(QColor(r, g, b))

        chart = QChart()
        chart.setBackgroundBrush(QBrush(QColor(79,79,79)))
        chart.addSeries(series)
        chart.legend().hide()
        self.chart_view.setChart(chart)

    def zoomin(self):
        if self.view_rect:
            self.view_rect.setHeight(self.view_rect.height() / 1.2)
            self.view_rect.setWidth(self.view_rect.width() / 1.2)
            self.ui.catchcamView.fitInView(self.view_rect, mode=Qt.KeepAspectRatio)
    
    def zoomout(self):
        if self.view_rect:
            self.view_rect.setHeight(self.view_rect.height() * 1.2)
            self.view_rect.setWidth(self.view_rect.width() * 1.2)
            self.ui.catchcamView.fitInView(self.view_rect, mode=Qt.KeepAspectRatio)
    
    def zoomfit(self):
        self.ui.catchcamView.fitInView(self.img_rect, mode=Qt.KeepAspectRatio)

    def select_area(self, rect, point):
        if rect.isNull():
            if self.view_rect:
                self.roi = self.view_rect
                self.scene.set_mode(SceneMode.ADD)
                self.ui.select.setChecked(False)
        else:
            p =  self.ui.catchcamView.mapToScene(rect)
            self.view_rect = p.boundingRect()

    def toggle_delete(self, b):
        if self.ui.deletepoints.isChecked():
            if self.ui.select.isChecked():
                self.ui.select.setChecked(False)
            self.scene.set_mode(SceneMode.DEL)
        else:
            self.ui.catchcamView.setDragMode(QGraphicsView.NoDrag)
            self.scene.set_mode(SceneMode.ADD)

    def toggle_select(self, b):
        if self.ui.select.isChecked():
            if self.ui.deletepoints.isChecked():
                self.ui.deletepoints.setChecked(False)
            if self.roi_item:
                self.scene.removeItem(self.roi_item)
                self.roi_item = None
            self.ui.catchcamView.setDragMode(QGraphicsView.RubberBandDrag)
            self.scene.set_mode(SceneMode.ROI)
        else:
            self.ui.catchcamView.setDragMode(QGraphicsView.NoDrag)
            self.scene.set_mode(SceneMode.ADD)

    def next(self):
        if self.file_fault:
            next_img = self.file_fault.next()
            self.update_img(next_img)

    def prev(self):
        if self.file_fault:
            prev_img = self.file_fault.prev()
            self.update_img(prev_img)

    def update_img(self, img):
        if img:
            tags = self.__model.set_target(img)
            roi = self.roi
            self.scene.clear()
            if roi:
                self.__show_roi(roi)
            for m in tags:
                self.scene.add_tag(m) 
            self.current_file = img
            self.ui.fileindex.setText(self.file_fault.index()) 
        else:
            self.ui.fileindex.setText("")
        
    @property
    def roi(self):
        r = self.__model.roi
        if r:
            return QRectF(r[0], r[1], r[2], r[3])
        else: 
            return None

    @roi.setter
    def roi(self, rect):
        if rect:
            r = (rect.x(),
                 rect.y(),
                 rect.width(),
                 rect.height())
            self.__model.roi = r
            self.__show_roi(rect)

    def __show_roi(self, rect):
        self.roi_item = QGraphicsRectItem(rect)
        self.roi_item.setPen(QColor(255,0,0))
        self.scene.addItem(self.roi_item)
        self.ui.catchcamView.fitInView(rect, mode=Qt.KeepAspectRatio)

    @property
    def current_file(self):
       return self.__current_file 

    @current_file.setter
    def current_file(self, filename):
        self.__current_file = filename
        image = QPixmap(self.current_file)
        self.scene.set_background(image)
        self.img_rect =  QRectF(image.rect())
        if not self.roi:
            self.roi = self.img_rect
        self.view_rect = self.roi
        self.scene.setSceneRect(self.img_rect)
        self.ui.catchcamView.fitInView(self.view_rect, mode=Qt.KeepAspectRatio)
        self.__has_image = True

    def select_species(self, species):
        self.__model.select(species)

    def browse(self):
        directory = QFileDialog.getExistingDirectory(self, "CatchCam Image directory")
        if directory:
            self.directory = directory
            self.report_filename = os.path.join(directory, 'composition.csv')
            self.pos_filename = os.path.join(directory, '.pos.dat')
            self.file_fault = FileFault(self.directory, ext="jpg;jpeg;png".split(';'))
            self.ui.fileindex.setText(self.file_fault.index()) 
            self.ui.cwd.setText(self.directory)
            if os.path.exists(self.pos_filename):
                self.load()
                self.__enable_toolbox(True)
                self.file_fault.goto(self.__model.last_edited)

            self.update_img(self.file_fault.current())

    def save(self):
        if self.pos_filename:
            self.__model.save_tags(self.pos_filename)
            self.__model.save_report(self.report_filename)

    def load(self):
        if self.pos_filename:
            self.scene.clear()
            self.__model.load_tags(self.pos_filename)

class SceneMode(enum.Enum):
    ADD,ROI,DEL=range(3)

class CatchTagScene(QGraphicsScene):
    
    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.background = None
        self.model = model
        self.bg_rect = None
        self.view_rect = None
        self.delete_radius = 16
        self.__mode = SceneMode.ADD

    def set_mode(self, mode):
        self.__mode = mode

    def drawBackground(self, painter, rect):
        if self.background:
            painter.drawPixmap(QRect(0,0, self.width(), self.height()), 
                self.background)

    def set_background(self, qpixmap):
        """
            set new background image
        """
        self.background = qpixmap
        self.bg_rect = QRectF(qpixmap.rect())
        self.view_rect = self.bg_rect 
        self.update()

    def add_tag(self, m):
        if m:
            item = SpeciesTagItem(m)
            item.setFlags(QGraphicsItem.ItemIsSelectable)
            self.addItem(item)


    def __get_tags(self, pos):
        r = self.delete_radius
        tag_rect = QRectF(pos.x() - r/2, pos.y() - r/2, r, r)
        return self.items(tag_rect)

    def mousePressEvent(self, event):
        pos = event.scenePos()
        if self.__mode == SceneMode.DEL:
            tags = self.__get_tags(pos)
            for tag in tags:
                if isinstance(tag, SpeciesTagItem):
                    model_uuid = tag.model_id()
                    if self.model.delete(model_uuid):
                        self.removeItem(tag)
        elif self.__mode == SceneMode.ADD:
            model = self.model.add(pos)
            if self.model:
                self.add_tag(model)


class SpeciesTagItem(QGraphicsItem):
    def __init__(self, model, parent=None):
        super().__init__(parent)
        p = QPointF(model.pos.x(), model.pos.y())
        self.setPos(p)
        self.__model = model
        #self.__model.active(self.active)
        #self.__model.deactive(self.deactive)
        self.__active = self.__model.is_active()
        r = self.__model.diameter 
        print(r)
        self.rect = QRectF(-r/2, -r/2, r, r)
    
    def species(self):
        return self.__model.species

    def model_id(self):
        return self.__model.get_id()

    def paint(self, painter, option, widget=None):
        painter.setPen(Qt.NoPen)
        r,g,b = self.__model.color
        color0 = QColor(r, g, b)
        color1 = QColor(255,255, 255)
        grad = QRadialGradient(0, 0, self.__model.diameter)
        grad.setColorAt(0, color0)
        grad.setColorAt(1, color1)
        brush = QBrush(grad)
        painter.setBrush(brush)
        painter.drawEllipse(self.rect)
        
    def boundingRect(self):
        return self.rect

    def shape(self):
        path = QPainterPath()
        path.addEllipse(self.rect)
        return path


if __name__ == '__main__':
    app = QApplication(sys.argv)
    model = SpeciesTagsList("species.lst")
    main = QMainWidget(Ui_CatchTag, model)
    main.show()
    app.exec_()
