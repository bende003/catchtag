"""
    This file is part of CatchTag.

    CatchTag is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CatchTag is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import glob
import csv
import uuid

from collections import Counter
from collections import defaultdict

def between(value, interval):
    return value > interval[0] and value < interval[1]

def inside(p, rect):
    x = p.x()
    y = p.y()
    if between(x, (rect[0], rect[0] + rect[2])) \
        and between(y, (rect[1], rect[1] + rect[3])):
            return True
    return False


class Point(object):
    def __init__(self, p):
        self.__x = p[0]
        self.__y = p[1]

    def x(self):
        return self.__x

    def y(self):
        return self.__y



class FileFault(object):
    def __init__(self, directory, ext=None):
        self.directory = directory
        self.files = []
        if isinstance(ext, list):
            for e in ext:
                wc = "*.%s" % e
                self.files.extend(glob.glob(os.path.join(self.directory, wc)))
        else:
            self.files = glob.glob(self.directory)

        self.files = [os.path.basename(p) for p in self.files]
        self.nb_files = len(self.files)

        self.__current = 0

    def current(self):
        try:
            return os.path.join(self.directory, self.files[self.__current])
        except IndexError:
            return None

    def next(self):
        l = len(self.files)
        if l > 0:
            self.__current += 1
            if not self.__current < l:
                self.__current = l - 1
            return os.path.join(self.directory, self.files[self.__current])
        return None

    def prev(self):
        l = len(self.files)
        if l > 0:
            self.__current -= 1
            if not self.__current >= 0:
                self.__current = 0
            return os.path.join(self.directory, self.files[self.__current])
        return None

    def index(self):
        return "%d/%d" %(self.__current + 1, self.nb_files)

    def goto(self, filename):
        self.__current = self.files.index(os.path.basename(filename))

class SpeciesTagModel(object):
    def __init__(self, species, pos, color, img_filename, diameter=16, _id = None) :
        super().__init__()
        if _id is None:
            self.__id = uuid.uuid1()
        else:
            self.__id = _id
        self.__color = color
        self.__img_filename = img_filename
        self.__species = species
        self.__pos = pos
        self.__diameter = diameter
        self.__active = False
        self.__active_cb = None
        self.__deactive_cb = None

    @property
    def pos(self):
        return self.__pos

    def deactive(self, cb):
        """
        set callback for activate
        """
        self.__deactive_cb = cb

    def active(self, cb):
        """
        set callback for activate
        """
        self.__active_cb = cb


    def activate(self):
        """
        activates Tag, and calls self.__active_cb if set.
        """
        self.__active = True
        if self.__active_cb:
            self.__active_cb()

    def deactivate(self):
        """
        activates Tag, and calls self.__active_cb if set.
        """
        self.__active = False
        if self.__deactive_cb:
            self.__deactive_cb()

    def get_id(self):
        return self.__id

    def is_active(self):
        return self.__active

    @property
    def diameter(self):
        return self.__diameter

    @property
    def color(self):
        return self.__color

    @property
    def species(self):
        return self.__species
    
    @property
    def header(self): 
        r = self.to_row()
        return list(r.keys())

    def __str__(self):
        return "%s, %f, %f, %s" % (self.__species, self.__pos.x(), self.__pos.y(), str(self.__id))

class SpeciesTagsList(object):
    def __init__(self, speciesfile):
        super().__init__()
        self.__species_list = SpeciesList(speciesfile)
        self.__species = None
        self.tags = defaultdict(list)
        self.rois = {}
        self.__target = None

    def set_target(self, target):
        if target is not None:
            self.__target = target
            return self.tags[target]

    def select(self, species):
        s = str(species).strip().lower().capitalize()
        if s in self.__species_list:
            self.__species = s
        else:
            self.__species = None

    @property
    def last_edited(self):
        return self.__last_edited

    @property
    def roi(self):
        if self.__target in self.rois:
           return self.rois[self.__target]
        else:
            return None

    @roi.setter
    def roi(self, rect):
        if self.__target:
            self.rois[self.__target] = rect
    @property
    def species_list(self):
        return self.__species_list
 
    def add(self, pos, _uuid=None):
        if self.__species and self.__target:
            if self.__target in self.rois:
                roi = self.rois[self.__target]
                if not inside(pos, self.rois[self.__target]):
                    return None
            else:
                roi = None
            color = self.__species_list.get_color(self.__species)
            count = len(self.tags[self.__target])
            model = SpeciesTagModel(self.__species, pos, color, self.__target, 16, _uuid)
            self.tags[self.__target].append(model)
            return model
        return None

    def delete(self, _uuid):
        for tag in self.tags[self.__target]: #short lists so lets use bf search
            if tag.get_id() == _uuid:
                self.tags[self.__target].remove(tag)
                return True
        return False
        
    def count_all(self):
        if self.tags:
            d = [f.species for f in self]
            count = Counter(d)
            return dict(count).items()
        return []
    
    def count_current(self):
        if self.__target:
            d = [f.species for f in self.tags[self.__target]]
            count = Counter(d)
            return dict(count).items()
        return []

    def __iter__(self):
        for k in self.tags.keys():
            for m in self.tags[k]:
                yield m

    def save_report(self, filename):
        with open(filename, 'w') as f:
            f.writelines(["%s,%d\n" % (i[0], i[1]) for i in self.count_all()])

    def save_tags(self, filename):
        with open(filename, 'w') as f:
            if self.tags:
                for t in self.tags:
                    if t in self.rois:
                        roi = str(self.rois[t])
                    else: 
                        roi = ""
                    f.write("<%s|%s>\n" % (t, roi))
                    f.writelines(["%s\n" % str(m) for m in self.tags[t]])


    def load_tags(self, filename):
        with open(filename, 'r') as f:
            self.clear()
            for l in f.readlines():
                l = l.strip()
                if l:
                    if l.startswith("<"):
                        l = l.strip("<")
                        l = l.strip(">")
                        tokens = l.split('|')
                        if len(tokens) > 1:
                            target = tokens[0]
                            self.__last_edited = target
                            roi = tuple([float(v) for v in tokens[1].strip('()').split(',')])
                            self.set_target(target)
                            self.roi = roi
                    else:
                        items = l.split(',')
                        if len(items) > 3:
                            species, x, y, _uuid = items
                        else:
                            species, x, y = items
                            _uuid = None
                        self.select(species)
                        pos = (float(x), float(y))
                        self.add(Point(pos), _uuid)

    def clear(self):
        self.__species = None
        self.tags = defaultdict(list)
        self.rois = {}
        self.__target = None

class SpeciesList(list):
    def __init__(self, speciesfile):
        super().__init__()
        self.filename = speciesfile
        self.color = {}
        with open(speciesfile, 'r') as f:
            for line in f:
                try:
                    s, c = line.split(';')
                    species = str(s).strip().lower().capitalize()
                    color = tuple([int(i) for i in c.strip().strip('()').split(',')])
                    self.append(species)
                    self.color[species] = color
                except ValueError:
                    pass

    def get_color(self, species):
        s = str(species).strip().lower().capitalize()
        try:
            return self.color[s]
        except KeyError :
            return (255,255,255) # default color is white

    def clear():
        del self[:]
        self.color = {}
